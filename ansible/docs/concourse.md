./concourse_linux_amd64 web \
  --basic-auth-username concourse \
  --basic-auth-password changeme \
  --external-url http://ec2-54-152-202-193.compute-1.amazonaws.com:8080 \
  --postgres-data-source postgres://concourse:changeme@10.0.0.12:5432/concourse?sslmode=disable \
  --tsa-authorized-keys concourse/docker/keys/web/authorized_worker_keys \
  --tsa-host-key concourse/docker/keys/web/tsa_host_key \
  --session-signing-key concourse/docker/keys/web/session_signing_key

sudo ./concourse_linux_amd64 worker \
  --work-dir /opt/concourse/worker \
  --tsa-host 127.0.0.1 \
  --tsa-public-key concourse/docker/keys/worker/tsa_host_key.pub \
  --tsa-worker-private-key concourse/docker/keys/worker/worker_key